jtartQage, a start page in jQuery
=================================

Features
--------

* Control your privacy.
* Static start page, no PHP, etc.

Installation
------------

* Copy everything where you want.
* (optional) Change path to jQuery, if you want use a CDN,
  check [CDN Hosted jQuery on jquery.com](http://docs.jquery.com/Downloading_jQuery#CDN_Hosted_jQuery)
* Copy bookmarks.xml.example to bookmarks.xml and hack it.
* Serve it through a web server, you can't use files without serving them due to the
  [same origin policy](http://en.wikipedia.org/wiki/Same_origin_policy).  
  You can run `python util/minimal_http_server.py` and use [localhost:12345](http://localhost:12345) to test.
* Create a js/analytics.js if you use Piwik or Google Analytics, it will be
  called in index.html.

bookmark.xml
------------
In configuration :

* jQueryEffect : tabs or accordion
* switchEvent : event to switch : mouseover, click or click hoverintent
* urlWithoutWWW : do not display 'www.'
* faviconShow : show favicon or not
* faviconDefaultHeight : set a favicon height
* faviconDefaultWidth : set a favicon width

`<group name="name of the group displayed in tabs">  
  <bookmark t="title" f="favicon.ico">url</bookmark>  
</group>`

* title : title displayed. If not, url is displayed.
* favicon : if not, try to display url/favicon.ico. If alpha.ico, use the included
  favicon/alpha.ico which is transparent.
* url : the bookmark url.

Notes
-----

* HTTP address must be escape or the XML will be not parsed.  
  Bad  : `http://example.com/index.php?option1=1&option2=2`  
  Good : `http://example.com/index.php?option1=1&amp;option2=2`
* Use `f="alpha.ico"` in links with authentification.  
  It will use favicon/alpha.ico, instead of retrieving a favicon which need
  authentification and risk blacklisting.
