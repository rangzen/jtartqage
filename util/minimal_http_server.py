import SimpleHTTPServer
import SocketServer

PORT = 12345
Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
httpd = SocketServer.TCPServer(("", PORT), Handler)

print "Serving on http://localhost:{}".format(PORT)

httpd.serve_forever()
