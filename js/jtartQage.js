/**
 * jtartQage.js
 * A start page in jQuery.
 *
 * Copyright (c) 2012 Cedric L'homme (http://l-homme.com)
 *
 * This file is part of jtartQage.
 * jtartQage is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * jtartQage is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with jtartQage.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @version 0.1
 *
 * @history 0.1 2012-02-18 First working day :)
 * @history 0.0 2012-01-01 Never worked
 *
 * @todo Check TODO.md
 */

// Default configuration
var xmlURI = 'bookmarks.xml';   // main configuration file

var jQueryEffect = "tabs";      // tabs or accordion
var switchEvent = "click hoverintent";  // event to switch : mouseover, click or
                                // click hoverintent
var urlWithoutWWW = true;       // do not display 'www.'
var faviconShow = true;         // show favicon or not
var faviconDefaultHeight = 16;  // set a favicon height
var faviconDefaultWidth = 16;   // set a favicon width

// Find root domain of an url
function url2a(url) {
    "use strict";
    var a = document.createElement('a');
    a.href = url;
    return a;
}

// Load configuration
function load_configuration(xml) {
    "use strict";

    // Check globals for meaning
    jQueryEffect = $(xml).find('jQueryEffect').text();
    switchEvent = $(xml).find('switchEvent').text();
    urlWithoutWWW = $(xml).find('urlWithoutWWW').text();
    faviconShow = $(xml).find('faviconShow').text();
    faviconDefaultHeight = $(xml).find('faviconDefaultHeight').text();
    faviconDefaultWidth = $(xml).find('faviconDefaultWidth').text();
}

// Generate bookmarks
function generate_bookmarks(xml) {
    "use strict";

    // Create the tabs list
    if (jQueryEffect === "tabs") {
        $('<ul id="bookmarksTabs"></ul>').appendTo("#bookmarks");
    }

    // Create each group
    $(xml).find('group').each(function () {
        var groupName = $(this).attr('name');
        // Tab
        if (jQueryEffect === "tabs") {
            $('<li><a href="#' + groupName + '">' + groupName +
                '</a></li>').appendTo("#bookmarksTabs");
        } else if (jQueryEffect === "accordion") {
            $('<h3><a href="#">' + groupName +
                '</a></h3>').appendTo("#bookmarks");
        }

        // Content and links
        if (jQueryEffect === "tabs") {
            $('<div id="' + groupName + '"><ul class="contentList" id="' +
                groupName + 'Content"></ul></div>').appendTo('#bookmarks');
        } else if (jQueryEffect === "accordion") {
            $('<div><ul class="contentList" id="' + groupName +
                'Content"></ul></div>').appendTo('#bookmarks');
        }
        $(this).find('bookmark').each(function () {
            var link = [], url, title, favicon;
            url = $(this).text();
            url = url2a(url);
            title = $(this).attr('t');
            favicon = $(this).attr('f');
            if (title === undefined || title === "") {
                title = url.hostname;
            }
            if (urlWithoutWWW && title.substring(0, 4) === "www.") {
                title = title.substring(4);
            }
            link.push('<li class="contentItem">');
            link.push('<a class="contentLink" href="' + url.href + '">');
            if (faviconShow === true || faviconShow === "true") {
                link.push('<img class="contentFavicon" src="');
                if (favicon === undefined) {
                    link.push(url.protocol + '//' + url.hostname +
                        '/favicon.ico"');
                } else {
                    var split = favicon.split(':');
                    if (split[0] === "local") {
                        link.push('favicon/' + split[1] + '"');
                    } else {
                        link.push(favicon + '"');
                    }
                }
                link.push('height="' + faviconDefaultHeight + '" width="' +
                    faviconDefaultWidth + '">');
            }
            link.push('<span class="contentTitle">' + title +
                '</span></a></li>');
            $(link.join("")).appendTo('#' + groupName + "Content");
        });
    });

    // jQuery magic
    if (jQueryEffect === "tabs") {
        $("#bookmarks").tabs({
            event: switchEvent
        });
    } else if (jQueryEffect === "accordion") {
		$("#bookmarks").accordion({
			event: switchEvent
		});
    }
}

// Central function
function main() {
    "use strict";
    $.get(xmlURI, function (xml) {
        load_configuration(xml);
        generate_bookmarks(xml);
    })
        .error(function () {$("<p></span>Can''t open or parse : <strong>" + xmlURI +
            "</strong><br />Check installation.</p>").appendTo("body"); });
}

// Da hook
$(document).ready(function () {
    "use strict";
    main();
});

// Hoverintent configuration
var cfg = ($.hoverintent = {
    sensitivity: 7,
    interval: 50
});

// Hoverintent behavior
$.event.special.hoverintent = {
    setup: function () {
        $(this).bind("mouseover", jQuery.event.special.hoverintent.handler);
    },
    teardown: function () {
        $(this).unbind("mouseover", jQuery.event.special.hoverintent.handler);
    },
    handler: function (event) {
        var self = this,
            args = arguments,
            target = $(event.target),
            cX,
            cY,
            pX,
            pY;

        function track(event) {
            cX = event.pageX;
            cY = event.pageY;
        }
        pX = event.pageX;
        pY = event.pageY;
        function clear() {
            target
                .unbind("mousemove", track)
                .unbind("mouseout", arguments.callee);
            clearTimeout(timeout);
        }
        function handler() {
            if ((Math.abs(pX - cX) + Math.abs(pY - cY)) < cfg.sensitivity) {
                clear();
                event.type = "hoverintent";
                jQuery.event.handle.apply(self, args);
            } else {
                pX = cX;
                pY = cY;
                timeout = setTimeout(handler, cfg.interval);
            }
        }
        var timeout = setTimeout(handler, cfg.interval);
        target.mousemove(track).mouseout(clear);
        return true;
    }
};
